import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-text-stats',
  templateUrl: './text-stats.component.html',
  styleUrls: ['./text-stats.component.css']
})
/**
 * Display the statistics for given input
 * @implements {OnInit}
 */
export class TextStatsComponent implements OnInit {
  
  text: string = '';
  hasText: boolean = false;
  wordCount: number = 0;
  vowels: number = 0;
  articles: number = 0;
  longestWordLength: number = 0;
  shortestWordLength: number = 1000;

  constructor() { }

  ngOnInit() {
  }

  /**
   * Find the vowels in the string
   * @returns {Number} vowelsCount 
   */
  findVowels(){
    let vowelsCount = 0;
    let vowelsList = ['a', 'e', 'i', 'o', 'u'];
    for(let ch of this.text){
      if(vowelsList.indexOf(ch) !== -1) vowelsCount++
    }
    return vowelsCount
  }

  /**
   * Find the number of articles in the string
   * @returns {Number} articlesCount 
   */
  findArticles(){
    let words = this.text.toLowerCase().split(' ');
    let articlesList = ['a', 'an', 'the']
    let articlesCount = 0;
    words.forEach(word => {
      if(articlesList.indexOf(word) != -1) articlesCount++;
    });
    return articlesCount;
  }

  /**
   * Find the longest and shortest word length in the string
   * @returns {Object}
   */
  findWordLength(){
    let shortlen = 1000;
    let largelen = 0;
    let articlesList = ['a', 'an', 'the']
    let words = this.text.trim().split(' ');
    words.forEach(word => {
      if(articlesList.indexOf(word) == -1){
        shortlen = word.length < shortlen ? word.length : shortlen
      }
      largelen = word.length > largelen ? word.length : largelen 
    })
    return { shortlen , largelen }
  }

  /**
   * Update the html when new string is entered 
   * @param evt  
   */
  onChange(evt: any){
  
    this.text = evt;
    this.hasText = this.text.trim().length > 0 ? true: false;
    this.wordCount = this.text.trim().replace(/\s\s+ | .+ /g, ' ').split(' ').length;
    this.vowels = this.findVowels();
    this.articles = this.findArticles();
    let result = this.findWordLength()
    this.shortestWordLength = result.shortlen;
    this.longestWordLength = result.largelen 
  }

  pasteUrl(e){
    console.log(e);
  }
}
