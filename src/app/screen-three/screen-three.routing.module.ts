import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TextStatsComponent } from './text-stats/text-stats.component';


const childRoutes: Routes = [
    {   
        path:'',
        component: TextStatsComponent 
    }
    // {
    //     path: "**",
    //     redirectTo: "/"
    // }
]

@NgModule({
    imports: [RouterModule.forChild(childRoutes)],
    exports: [RouterModule]
})

export class ScreenThreeRoutingModule { }