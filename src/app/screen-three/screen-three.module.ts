import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MatInputModule } from '@angular/material/input';

import { TextStatsComponent } from './text-stats/text-stats.component';
import { ScreenThreeRoutingModule } from './screen-three.routing.module'


@NgModule({
  declarations: [TextStatsComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    ScreenThreeRoutingModule
  ]
})
export class ScreenThreeModule { }
