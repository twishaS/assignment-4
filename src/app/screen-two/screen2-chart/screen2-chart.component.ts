import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

import { Chart } from 'angular-highcharts';

import data from '../../../assets/screen2-data.json';

@Component({
  selector: 'app-screen2-chart',
  templateUrl: './screen2-chart.component.html',
  styleUrls: ['./screen2-chart.component.css']
})
/**
 * Handle the charts on second screen
 * @implements {OnInit}
 */
export class Screen2ChartComponent implements OnInit {

  /**
   * declare line chart and area chart variables
   */
  lineChart = new Chart(<any>data.lineChartOptions);
  areaChart = new Chart(<any>data.areaChartOptions);

  // To store the 
  myInterval: any;

  constructor() {
  }

  ngOnInit() {
  }

  // add point to chart serie
  addNewPoint(self){
    let newPoint = Math.floor(Math.random() * 10)
    //console.log(self.lineChart)
    self.lineChart.addPoint(newPoint);

    self.areaChart.addPoint(newPoint);

    // update json file
    data.lineChartOptions.series[0].data.push(newPoint);
    data.areaChartOptions.series[0].data.push(newPoint);
  }

  /**
   * On Add button click add new points every one
   */
  addClick() {
    let self = this;
    this.addNewPoint(self)

    this.myInterval = interval(1000).subscribe(val=>{
      console.log(val)
      this.addNewPoint(self)
    })
  }
  /**
   * Stop adding numbers when stop button is clicked
   */
  stopClick(){
    this.myInterval.unsubscribe();
  }

}
