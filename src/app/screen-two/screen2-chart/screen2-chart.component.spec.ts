import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Screen2ChartComponent } from './screen2-chart.component';

describe('Screen2ChartComponent', () => {
  let component: Screen2ChartComponent;
  let fixture: ComponentFixture<Screen2ChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Screen2ChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Screen2ChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
