import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'


import { Screen2ChartComponent } from './screen2-chart/screen2-chart.component';
import { ScreenTwoRoutingModule } from './screen-two.routing.module'

import { ChartModule } from 'angular-highcharts';

import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [Screen2ChartComponent],
  imports: [
    CommonModule,
    ScreenTwoRoutingModule,
    ChartModule,
    MatButtonModule
  ]
})
export class ScreenTwoModule { }
