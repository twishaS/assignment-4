import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Screen2ChartComponent } from './screen2-chart/screen2-chart.component';


const childRoutes: Routes = [
    {   
        path:'',
        component: Screen2ChartComponent 
    }
    // {
    //     path: "**",
    //     redirectTo: "/"
    // }
]

@NgModule({
    imports: [RouterModule.forChild(childRoutes)],
    exports: [RouterModule]
})

export class ScreenTwoRoutingModule { }