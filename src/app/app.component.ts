import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-project';

  /**
   * Define routes for each tab
   */
  navLinks = [{
    path: 'tab1',
    label: 'Population Charts'
  },
  {
    path: 'tab2',
    label: 'Line Chart'
  },
  {
    path: 'tab3',
    label: 'Text Statistics'
  }
  ]
}
