import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'tab1',
    loadChildren: './screen-one/screen-one.module#ScreenOneModule'
  },
  {
    path: 'tab2',
    loadChildren: './screen-two/screen-two.module#ScreenTwoModule'
  },
  {
    path: 'tab3',
    loadChildren: './screen-three/screen-three.module#ScreenThreeModule'
  },
  {
    path: '**',
    redirectTo:'/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
