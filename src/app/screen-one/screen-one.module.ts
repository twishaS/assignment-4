import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule , ReactiveFormsModule}   from '@angular/forms';

import { Screen1ChartsComponent } from './screen1-charts/screen1-charts.component';
import { ScreenOneRoutingModule } from './screen-one.routing.module'

import { ChartModule } from 'angular-highcharts';

import {MatButtonModule, MatInputModule, MatFormFieldModule, MatSelectModule} from '@angular/material';

@NgModule({
  declarations: [Screen1ChartsComponent],
  imports: [
    CommonModule,
    ScreenOneRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ChartModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
  ]
})
export class ScreenOneModule { }
