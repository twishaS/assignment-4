export const barChartOptions =  {
        "chart": {
            "type": "bar"
        },
        "title": {
            "text": "Historic World Population by Region"
        },
        "subtitle": {
            "text": "Source: <a href='https://en.wikipedia.org/wiki/World_population'>Wikipedia.org</a>"
        },
        "xAxis": {
            "categories": [
                "Africa",
                "America",
                "Asia",
                "Europe",
                "Oceania"
            ],
            "title": {
                "text": null
            }
        },
        "yAxis": {
            "min": 0,
            "title": {
                "text": "Population (millions)",
                "align": "high"
            },
            "labels": {
                "overflow": "justify"
            }
        },
        "tooltip": {
            "valueSuffix": "millions"
        },
        "plotOptions": {
            "bar": {
                "dataLabels": {
                    "enabled": true
                }
            }
        },
        "credits": {
            "enabled": false
        },
        "series": [
            {
                "name": "Year 1800",
                "data": [
                    107,
                    31,
                    635,
                    203,
                    2
                ]
            }
        ]
    }
export const columnChartOptions =  {
        "chart": {
            "type": "column"
        },
        "title": {
            "text": "Historic World Population by Region"
        },
        "subtitle": {
            "text": "Source: <a href='https://en.wikipedia.org/wiki/World_population'>Wikipedia.org</a>"
        },
        "xAxis": {
            "categories": [
                "Africa",
                "America",
                "Asia",
                "Europe",
                "Oceania"
            ],
            "title": {
                "text": null
            }
        },
        "yAxis": {
            "min": 0,
            "title": {
                "text": "Population (millions)",
                "align": "high"
            },
            "labels": {
                "overflow": "justify"
            }
        },
        "tooltip": {
            "valueSuffix": "millions"
        },
        "plotOptions": {
            "column": {
                "dataLabels": {
                    "enabled": true
                }
            }
        },
        "credits": {
            "enabled": false
        },
        "series": [
            {
                "name": "Year 1800",
                "data": [
                    107,
                    31,
                    635,
                    203,
                    2
                ]
            }
        ]
    }
export const pieChartOptions = {
        "chart": {
            "plotBackgroundColor": null,
            "plotBorderWidth": null,
            "plotShadow": false,
            "type": "pie"
        },
        "title": {
            "text": "Browser market shares in January,2018"
        },
        "tooltip": {
            "pointFormat": "{series.name}: {point.percentage:.1f}%"
        },
        "plotOptions": {
            "pie": {
                "allowPointSelect": true,
                "cursor": "pointer",
                "dataLabels": {
                    "enabled": true
                },
                "showInLegend": true
            }
        },
        "series": [
            {
                "name": "Brands",
                "colorByPoint": true,
                "data": [
                    {
                        "name": "Chrome",
                        "y": 61,
                        "sliced": true,
                        "selected": true
                    },
                    {
                        "name": "Internet Explorer",
                        "y": 11
                    }
                ]
            }
        ]
    }
export const donutChartoptions = {
        "chart": {
            "type": "pie"
        },
        "title": {
            "text": "Browser market shares in January,2018"
        },
        "tooltip": {
            "pointFormat": "{series.name}: {point.percentage:.1f}%"
        },
        "plotOptions": {
            "pie": {
                "shadow": false,
                "center": ["50%", "50%"],
                "size": "80%",
                "innerSize": "55%",
                "showInLegend": true
            }
        },
        "series": [
            {
                "name": "Brands",
                "colorByPoint": true,
                "data": [
                    {
                        "name": "Chrome",
                        "y": 61,
                        "sliced": true,
                        "selected": true
                    },
                    {
                        "name": "Internet Explorer",
                        "y": 11
                    }
                ]
            }
        ]
    }