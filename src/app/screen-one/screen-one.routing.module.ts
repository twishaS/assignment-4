import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Screen1ChartsComponent } from './screen1-charts/screen1-charts.component';


const childRoutes: Routes = [
    {   
        path:'',
        component: Screen1ChartsComponent
    }
    // {
    //     path: "**",
    //     redirectTo: "/"
    // }
]

@NgModule({
    imports: [RouterModule.forChild(childRoutes)],
    exports: [RouterModule]
})

export class ScreenOneRoutingModule { }