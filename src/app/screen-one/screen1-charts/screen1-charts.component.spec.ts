import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Screen1ChartsComponent } from './screen1-charts.component';

describe('Screen1ChartsComponent', () => {
  let component: Screen1ChartsComponent;
  let fixture: ComponentFixture<Screen1ChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Screen1ChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Screen1ChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
