import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';

import { Chart } from 'angular-highcharts';

import {barChartOptions, columnChartOptions, pieChartOptions, donutChartoptions } from '../screen1-data';

@Component({
  selector: 'app-screen1-charts',
  templateUrl: './screen1-charts.component.html',
  styleUrls: ['./screen1-charts.component.css']
})
/**
 * Handle the 4 charts on the screen
 * @implements {OnInit}
 */
export class Screen1ChartsComponent implements OnInit {
 
  /**
   * Declare four different charts
   */
  public barChart = new Chart(<any>barChartOptions);
  public columnChart = new Chart(<any>columnChartOptions);
  pieChart = new Chart(<any>pieChartOptions);
  donutChart = new Chart(<any>donutChartoptions)

  /**
   * Declare form groups 
   */
  barChartForm: FormGroup;
  pieChartForm: FormGroup;

  constructor(private _formBuilder: FormBuilder) { 
  }

  ngOnInit() {
    this.barChartForm = this._formBuilder.group({
      'name': new FormControl(null, [Validators.required, Validators.pattern('([0-9],){5}')]),
      'data': new FormControl(null, Validators.required),
      'countryName': new FormControl(null),
      'population': new FormControl(null)
    })

    this.pieChartForm = this._formBuilder.group({
      'name': new FormControl(null, Validators.required),
      'y': new FormControl(null, Validators.required)
    })
  }
 
  /**
   * Add user input to bar and column chart  
   */
  addSeriesToBarAndColumn() {
    let name = this.barChartForm.value.name;
    console.log(name)
    let newSeries = {
      "name": name!= null ? name.trim(): null ,
      "data": JSON.parse(`[ ${this.barChartForm.value.data.trim()} ]`)
    };
    console.log(newSeries)

    if(newSeries.name!=null && typeof(newSeries.data)){
        this.barChart.addSeries(<any>newSeries, true, false)
       this.columnChart.addSeries(<any>newSeries, true, false)
    }
    
  }

  /**
   * 
   */
  // addPointToBarAndColumn(){
  //   let country = this.barChartForm.value.countryName;
  //   let population = parseInt(this.barChartForm.value.population);
  //   console.log(country,population)

  //   this.barChart.options.xAxis.categories.push(country);
  //   this.barChart.addPoint(population);
  //   console.log(this.barChart)
  //   this.columnChart.options.xAxis.categories.push(country);
  //   this.columnChart.addPoint(population);
  // }
  /**
   * Add user input to Pie and donut chart
   */
  addToPieAndDonut(){
    let newData = {
      "name": this.pieChartForm.value.name,
      "y": parseInt(this.pieChartForm.value.y)
    };

    console.log(newData)
    this.pieChart.addPoint(<any>newData)
    this.donutChart.addPoint(<any>newData)
  }
}
